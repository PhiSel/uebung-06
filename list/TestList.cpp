#include <unittest++/UnitTest++.h>
#include "List.hpp"
#include <iostream>

#include <vector>

// clear &&  g++ TestList.cpp -o TestList -lUnitTest++ -std=c++0x && ./TestList

SUITE(Test_List)
{
	TEST(Aufgabe_51)
	{
		List<int> liste;
		CHECK(liste.empty());
		CHECK_EQUAL(0, liste.size());
	}

	TEST(Aufgabe_52)
	{
		List<int> liste; 

		liste.push_front(42);				// 42
		CHECK_EQUAL(42, liste.front());

		liste.push_front(3);				// 3 42
		CHECK_EQUAL(3, liste.front());

		liste.push_back(49);				// 3 42 49
		CHECK_EQUAL(49, liste.back());

		CHECK_EQUAL(3, liste.size());

		liste.pop_front();					// 42 49
		CHECK_EQUAL(42, liste.front());

		liste.pop_front();					// 49
		CHECK_EQUAL(49, liste.front());
		CHECK_EQUAL(49, liste.back());
		CHECK_EQUAL(1, liste.size());


	}

	TEST(Aufgabe_53)
	{
		List<int> liste; 
		liste.push_front(1); 	// 1
		liste.push_front(2); 	// 2 1
		liste.push_front(3); 	// 3 2 1
		liste.push_front(4); 	// 4 3 2 1
		CHECK(!liste.empty());
		liste.clear(); 			// 
		CHECK(liste.empty());

	}

	TEST(Aufgabe_55)
	{
		// it_should_be_an_empty_range
		List<int> liste;
		auto b = liste.begin(); 
		auto e = liste.end(); 
		CHECK(b == e);

		// it_should_provide_acces_to_the_first_element_with_begin
		liste.push_front(42); 
		CHECK_EQUAL(42, *liste.begin());
	}

	TEST(Aufgabe57)
	{
		// it_should_have_an_copy_ctor
		List<int> liste;
		liste.push_front(1); 
		liste.push_front(2); 
		liste.push_front(3); 
		liste.push_front(4); 
		List<int> list2(liste); 
		CHECK(liste == list2);

		// it_should_have_an_move_copy_ctor
		List<int> list3(std::move(liste));
		CHECK_EQUAL(0, liste.size());
		CHECK(liste.empty());
		CHECK_EQUAL(4, list3.size());

	}

	TEST(Aufgabe58)
	{
		List<int> liste;
		liste.insert(liste.begin(),456);	// 456
		liste.insert(liste.end(), 999);		// 456 999
		CHECK(!liste.empty());
		CHECK_EQUAL(2,liste.size());
		CHECK_EQUAL(456, liste.front());
		CHECK_EQUAL(999, liste.back());

		liste.push_front(20);				// 20 456 999

		ListIterator<int> it = ++ ++ liste.begin();
		liste.insert(it, 5);				// 20 456 5 999

		int counter = 0;
		for (auto const& item : liste)
		{
			if (counter == 0) { CHECK_EQUAL(20, item); }
			if (counter == 1) { CHECK_EQUAL(456, item); }
			if (counter == 2) { CHECK_EQUAL(5, item); }
			if (counter == 3) { CHECK_EQUAL(999, item); }
			++counter;	
		}
		CHECK_EQUAL(20, liste.front());
		CHECK_EQUAL(999, liste.back());

	}

	TEST(Aufgabe59_memberfunction)
	{
		List<int> l1;
		l1.push_front(1); // 1
		l1.push_front(2); // 2 1
		l1.push_front(3); // 3 2 1
		l1.push_front(4); // 4 3 2 1

		l1.reverse(); // 1 2 3 4
		int counter = 0;
		for (auto const& item : l1) 
		{
			if (counter == 0) { CHECK_EQUAL(1, item); }
			if (counter == 1) { CHECK_EQUAL(2, item); }
			if (counter == 2) { CHECK_EQUAL(3, item); }
			if (counter == 3) { CHECK_EQUAL(4, item); }
			++counter;
		}
		CHECK_EQUAL(1, l1.front());
		CHECK_EQUAL(4, l1.back());

	}
	TEST(Aufgabe59_freefunction)
	{
		List<int> l1;
		l1.push_front(1); // 1
		l1.push_front(2); // 2 1
		l1.push_front(3); // 3 2 1
		l1.push_front(4); // 4 3 2 1

		auto l2 = reverse(l1); // 1 2 3 4
		int counter = 0;
		for (auto const& item : l2) 
		{
			if (counter == 0) { CHECK_EQUAL(1, item); }
			if (counter == 1) { CHECK_EQUAL(2, item); }
			if (counter == 2) { CHECK_EQUAL(3, item); }
			if (counter == 3) { CHECK_EQUAL(4, item); }
			++counter;
		}
		CHECK_EQUAL(1, l2.front());
		CHECK_EQUAL(4, l2.back());
	}

	TEST(Aufgabe510)
	{
		List<int> l1;
		l1.push_front(1); // 1
		l1.push_front(2); // 2 1
		l1.push_front(3); // 3 2 1
		l1.push_front(4); // 4 3 2 1

		auto l2 = l1;
		CHECK(l1 == l2);
	}

	TEST(Aufgabe511)
	{
		List<int> l1;
		l1.push_front(1); // 1
		l1.push_front(2); // 2 1
		l1.push_front(3); // 3 2 1
		l1.push_front(4); // 4 3 2 1

		std::vector<int> v1(l1.size());
		std::copy(l1.begin(), l1.end(), v1.begin());

		int counter = 0;

		for (auto const& item : v1)
		{
			if (counter == 0) { CHECK_EQUAL(4, item); }
			if (counter == 1) { CHECK_EQUAL(3, item); }
			if (counter == 2) { CHECK_EQUAL(2, item); }
			if (counter == 3) { CHECK_EQUAL(1, item); }
			++counter;
		}
	}

	TEST(list_swap)
	{
		List<int> l1;
		l1.push_front(1); // 1
		l1.push_front(2); // 2 1
		l1.push_front(3); // 3 2 1
		l1.push_front(4); // 4 3 2 1

		List<int> l2;
		l2.swap(l1);
		
		CHECK(l1.empty());
		CHECK(l2 != l1);
	}
}

int main(int argc, char const *argv[])
{	
	return UnitTest::RunAllTests();
}