#ifndef BUW_LIST_HPP 
#define BUW_LIST_HPP

#include <cstddef>
#include <assert.h>

/******************** 
* Noch zu erledigen *
*********************
*    Aufgabe 5.1    *	DONE!
*    Aufgabe 5.2    *	DONE!
*    Aufgabe 5.3    *	DONE!
*    Aufgabe 5.4    *	DONE!
*    Aufgabe 5.5    *	DONE!
*    Aufgabe 5.6    *	DONE!
*    Aufgabe 5.7    *	DONE!
*    Aufgabe 5.8    *	DONE!
*    Aufgabe 5.9    *	DONE!
*    Aufgabe 5.10   *	DONE!
********************/

// List.hpp

template <typename T>
class List;	// Notwendig für den Zugriff auf m_node für insert mittels friend in struct ListIterator !



template <typename T> 
struct ListNode // enthält Zeiger auf das vorherige, das nächste Element und hält ein Objekt vom Typ T.
{
	ListNode() : m_value(), m_prev(nullptr), m_next(nullptr) {} 

	ListNode(T const& v, ListNode* prev, ListNode* next)
		: m_value(v), m_prev(prev), m_next(next) {}

	T m_value;
	ListNode* m_prev; //
	ListNode* m_next; 
};



/**************
* Aufgabe 5.4 *
***************/
template <typename T> 
struct ListIterator
{
	friend class List<T>;	// Notwendig für den Zugriff auf m_node für insert mittels friend
	typedef ListIterator <T> Self;
	typedef T* Pointer;
	typedef T& Reference;
	typedef std::forward_iterator_tag IteratorCategory; 

	ListIterator ():
		m_node{} {} 

	ListIterator(ListNode<T>* n):
		m_node{n} {} 

	Reference operator*() const // Deferenzieren (Holen von verwiesenen Objekt selbst)
	{
		return m_node->m_value;
	} 

	Pointer operator->() const // Verweis mit einem Zeiger auf die Member des verwiesenen Objektes
	{
		return &(m_node->m_value);
	} 

	Self& operator++() //Präinkrement
	{
		if (m_node) { m_node = m_node->m_next; }	// Wenn es ein Knoten ist, dann setze den aktuellen Knotenzeiger auf den Nächsten
		return *this;
	} 

	Self operator++(int) // Postinkrement
	{
		auto temp = *this;
		++*this;
		return temp;
	} 

	bool operator==(const Self& x) const 
	{
		return m_node == x.m_node;
	}  

	bool operator!=(const Self& x) const 
	{
		return m_node != x.m_node;
	}  

private:
ListNode <T>* m_node; // Zeiger auf aktuelles Element!!!
};



template <typename T> 
class List
{
public:
	/**************
	* Aufgabe 5.1 *
	***************/
	List():	// Default-Konstruktor
		m_size{0},
		m_first{nullptr},
		m_last{nullptr} {}

	List(List<T> const& liste):	// Copy-Konstruktor
		m_size{0},				// Setze die Größe auf Null, sowie ..
		m_first{nullptr},		// den "Ersten Zeiger" ...
		m_last{nullptr}			// und "Letzten Zeiger" auf Nullpointer
		{
			#if 0
			auto temp = liste.m_last;			// Erstelle einen neuen Pointer auf das Listenende
			while(temp)							// Solange temp auf ein Objekt zeigt ...
			{
				push_front(temp -> m_value);	// füge den Wert des Elementes an den Anfang der neuen Liste ein ...
				temp = temp -> m_prev;			// und lasse temp auf das vorherige Element zeigen.
			}
			#endif
			for (auto const& x : liste) {
				push_back(x);
			}
		}

	List(List<T>&& liste):	// Move-Konstruktor
		m_first{liste.m_first},	// "Erster Zeiger" der neuen Liste zeigt auf das erste Element der alten Liste
		m_last{liste.m_last},	// "Letzter Zeiger" der neuen Liste zeigt auf das letzte Element der alten Liste 
		m_size(liste.m_size)
		{
			liste.m_first = nullptr;	// Setze den "Ersten Zeiger" der alten Liste auf den Nullpointer
			liste.m_last = nullptr;		// Setze den "Letzten Zeiger" der alten Liste auf den Nullpointer
		    liste.m_size = 0;
		}

	~List(){clear();};

	bool empty() const { return m_size == 0; }

	std::size_t size() const { return m_size; }

	/**************
	* Aufgabe 5.2 *
	***************/
	void push_front( T const& value )
	{
		auto temp = new ListNode<T>{value, nullptr, m_first};	// Erzeuge ein neues Listenelement mit einem Wert und 2 Zeigern
		// Die ersten beiden Befehle können weggelassen werden, da sie im Konstruktor schon vereinbart sind
		temp -> m_prev = nullptr;	//Setze den "Vorherigen Zeiger" des neuen 1. Elements auf den Nullppointer
		temp -> m_next = m_first;	//Setze den "Nächsten Zeiger" des neuen 1. Elements auf das alte 1. Element
		if (!m_first) 					// falls einziges Element ...
		{ 
			m_last = temp; 				// Setze den "Letzten Zeiger" auf das neue Element, ansonsten ...
		}else{ 
			m_first -> m_prev = temp;	// Setze "Vorherigen Zeiger" vom alten 1. Element auf das Neue
		}
		m_first = temp;				// Setze den "Ersten Zeiger" auf das neue 1. Element
		++m_size;					// Erhöhe die Anzahl der in der Liste vorhandenen Elemente um 1
	}

	void push_back( T const& value )
	{
		auto temp = new ListNode<T>{value, m_last, nullptr}; // Erzeuge ein neues Listenelement mit einem Wert und 2 Zeigern
		// Die ersten beiden Befehle können weggelassen werden, da sie im Konstruktor schon vereinbart sind
		temp -> m_prev = m_last;	// Setze den "Vorherigen Zeiger" des neuen letzten Elements auf das alte letzte Element
		temp -> m_next = nullptr;	// Setze den "nächsten Zeiger" des neuen letzten Elements auf den Nullpointer
		if (!m_last) 							// falls es das einzige Element ist ...
		{
			m_first = temp;						// Setze den "Ersten Zeiger" auf das neue Element, ansonsten ...
		}else{
			m_last -> m_next = temp;	// Setze vom alten "Letzten Zeiger" den "Nächsten Zeiger" auf das neue Element
		}
		m_last = temp;				// Setze den "Letzten Zeiger" auf das neue letzte Element
		++m_size;					//Erhöhe die Anzahl der in der Liste vorhanden Elemente um 1
	}

	void pop_front( )
	{
		assert(!empty());			// Überprüfen, ob der Zeiger nicht auf den Nullpointer zeigt
		auto temp = m_first;		// Erzeugt einen neuen Pointer auf das erste Element
		m_first = temp ->m_next;
		if (!m_first) 						// wenn kein weiteres Element vorhanden ist ...
		{
			m_last = nullptr; 				// setze den "Letzten Zeiger" auf den Nullpointer ...
		}else{
			m_first -> m_prev = nullptr; 	// ansonsten setze den den "Vorherigen Zeiger" des 1. Elements auf den Nullpointer
		}
		delete temp; 				// Lösche temp
		--m_size;
		
	}

	void pop_back( )
	{
		assert(!empty());			// Überprüfen, ob der Zeiger nicht auf den Nullpointer zeigt
		auto temp = m_last;			// Erzeuge einen neuen Pointer auf das letzte Element
		m_last = temp -> m_prev;	// Setze den "Letzten Zeiger" auf das vorhergehende Objekt
		if (!m_last)						// Wenn kein weiteres Element vorhanden ist ...
		{
			m_first = nullptr;				// setze den "Ersten Zeiger" auf den Nullpointer ...
		}else{
			m_last -> m_next = nullptr;		// ansonsten setze den "Nächsten Zieger" des letzten Elements auf den Nullpointer.
		}
		delete temp;				// Lösche temp
		--m_size;	
	}

	T front( ) const
	{ 
		assert(!empty());	// Überprüfen, ob der Zeiger nicht auf den Nullpointer zeigt
		return m_first->m_value; 	// Gib den Wert des ersten Elementes zurück
	}

	T back( ) const
	{ 
		assert(!empty());	// Überprüfen, ob der Zeiger nicht auf den Nullpointer zeigt
		return m_last -> m_value; 	// Gib den Wert des letzten Elementes zurück
	}


	/**************
	* Aufgabe 5.3 *
	***************/
	void clear( )
	{
		while(!empty())	// Solange die Liste nicht leer ist ...
		{ pop_front();}	// entferne immer das erste Element
	}

	/**************
	* Aufgabe 5.5 *
	***************/
	ListIterator<T> begin() const
	{
		return ListIterator<T>(m_first);
	}	

	ListIterator<T> end() const
	{
		return ListIterator<T>(nullptr);
	}

	/**************
	* Aufgabe 5.8 *
	***************/
	ListIterator<T> insert(ListIterator<T> const& pos, T const& value)
	{
		if (pos == begin()) // Einfügen am Anfang
		{
			push_front(value);
			return ListIterator<T> (m_first);
		}

		else if (pos == end()) // Einfügen am Ende
		{
			push_back(value);
			return ListIterator<T>(m_last);
		}

		else
		{
			// Erstelle einen neuen Knoten mit dem übergebenen Wert und den Zeigern auf das vorherige und aktuelle Element an der Position
			auto einfuegen = new ListNode<T>{value, pos.m_node->m_prev, pos.m_node};	
			einfuegen -> m_prev -> m_next = einfuegen;	// Setze den "nächsten Zeiger" des vorhergehenden Elements auf das Neue
			einfuegen -> m_next -> m_prev = einfuegen;	// setze den "vorherigen Zeiger" des nächsten Elements auf das neue
			++m_size;
			return ListIterator<T>(einfuegen);
		}

		
	}

	/**************
	* Aufgabe 5.9 *
	***************/
	void reverse()
	{
		auto ptr = m_first;		// Erstelle einen Zeiger ptr auf das "Erste Element".
		while(ptr != nullptr)	// Solange mein ptr kein Nullpointer ist ...
		{
			auto temp =  ptr -> m_next;		// Erstelle einen neuen Zeiger temp auf das Nächste Element ...
			ptr -> m_next = ptr -> m_prev;	// lass vom aktuellen Element den "Nächsten Zeiger" auf das "Vorherige" zeigen ...
			ptr -> m_prev = temp;			// und den "Vorherigen Zeiger" auf temp.
			if (temp == nullptr)				// Wenn temp der Nullpointer ist ...
			{
				m_last = m_first;				// dann setze den "Letzten Zeiger" auf den "Ersten Zeiger" ...
				m_first = ptr;					// und den "Ersten Zeiger" auf das aktuelle Element.
			}
			ptr = temp;						// Lass nun den aktuellen Zeiger auf temp (dem Nächsten Element) zeigen.
		}
		// in der while schleife std::swap(ptr->m_next, ptr->m_prev);
		// std::swap(m_first, m_last);
	}

	/***************
	* Aufgabe 5.10 *
	****************/
	List& operator=(List const& neu)
	{
		#if 0
		for(auto const& item : neu)
		{
			push_back(item);
		}
		#else
		swap(neu);
		#endif
		return *this;
	}

	void swap(List<T>& rhs)
	{
		std::swap(m_first, rhs.m_first);
		std::swap(m_last, rhs.m_last);
		std::swap(m_size, rhs.m_size);

	}

	friend void swap(List<T>& a, List<T>& b)
	{
		a.swap(b);
	}

private:
	std::size_t m_size; 
	ListNode <T>* m_first; 
	ListNode <T>* m_last;
};



/**************
* Aufgabe 5.6 *
***************/
template <typename T>
bool operator==(List<T> const& xs, List<T> const& ys)
{
	if (xs.size() != ys.size())	
	{
		return false;
	}
	auto a = xs.begin();
	auto b = ys.begin();
	while (a != b)
	{
		if (*a != *b) { return false; }
		++a;
		++b;
	}
	return true;
}


template <typename T>
bool operator!=(List<T> const& xs, List<T> const& ys)
{
	return (!operator==(xs, ys));
}


/**************
* Aufgabe 5.9 *
***************/
template <typename T>
List<T> reverse(List <T> const& oldlist)
{
	List<T> newlist{oldlist};
	newlist.reverse();
	return newlist;
}


#endif // #define BUW_LIST_HPP