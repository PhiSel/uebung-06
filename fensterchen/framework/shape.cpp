#include "shape.hpp"

std::string Shape::name() const
{
	return name_;
}

bool Shape::is_leaf() const
{
	return isleaf_;
}

/**************
* Aufgabe 6.4 *
***************/

std::ostream& Shape::print(std::ostream& os) const
{
	os 	<< "name: " << name_  << ","
		<< " color: (" << (int)color_.r_ << "," << (int)color_.g_ << "," << (int)color_.b_ << ")" ;
	return os;
}

std::ostream& operator << (std::ostream& os, Shape const& s)
{
	return s.print(os); 
}
