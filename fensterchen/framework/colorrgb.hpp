#ifndef BUW_COLORRGB_HPP 
#define BUW_COLORRGB_HPP

#include <iostream>

struct ColorRGB
{
	ColorRGB():
		r_{0},
		g_{0},
		b_{0}
		{
			// std::cout << "ctor struct ColorRGB\n";
		}

	ColorRGB(unsigned char const color):
		r_{color},
		g_{color},
		b_{color}
		{
			// std::cout << "ctor struct ColorRGB\n";
		}

	ColorRGB(unsigned char const r, unsigned char const g, unsigned char const b):
		r_{r},
		g_{g},
		b_{b}
		{
			// std::cout << "ctor struct ColorRGB\n";		
		}

	~ColorRGB(){/*std::cout << "dtor struct ColorRGB\n";*/}

	unsigned char r_,g_,b_;
};

#endif