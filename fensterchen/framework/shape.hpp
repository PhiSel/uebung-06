#ifndef BUW_SHAPE_HPP
#define BUW_SHAPE_HPP

#include "point2d.hpp"
#include "colorrgb.hpp"
#include "fensterchen.hpp"
#include <iostream>

class Shape
{
public:
	Shape():
		name_{"Shape"},
		color_{0},
		isleaf_{false}
		{
			// std::cout << "d_ctor class Shape " << name_ << "\n";
		}

	Shape(ColorRGB const& color, std::string const& name, bool leaf):
		name_{name},
		color_{color},
		isleaf_{leaf}
		{
			// std::cout << "ctor class Shape " << name_ << "\n";
		}

	virtual ~Shape(){/*std::cout << "dtor class Shape " << name_ << "\n";*/}

	std::string name() const;

	/**************
	* Aufgabe 6.1 *
	***************/
	virtual void draw(Window const& win) const = 0;
	virtual void draw(Window const& win, ColorRGB const& clr) const = 0;
	virtual bool is_inside(Point2d const& p) const = 0;
	bool is_leaf() const;
	virtual void draw_leaf(Window const& win, Point2d const& ptr, ColorRGB const& clr) const{};
	virtual void remove(std::string const& name) const {};

	/**************
	* Aufgabe 6.4 *
	***************/
	virtual std::ostream& print(std::ostream& os) const;
	
protected:
	ColorRGB color_;
	std::string name_;
	bool isleaf_;
private:

};

std::ostream& operator << (std::ostream& os, Shape const& s);

#endif //BUW_SHAPE_HPP