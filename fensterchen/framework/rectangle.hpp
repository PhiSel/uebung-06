#ifndef BUW_RECTANGLE_HPP
#define BUW_RECTANGLE_HPP 

#include "shape.hpp"

class Rectangle : public Shape
{
public:
	Rectangle():
		Shape{{0},"Rectangle", true},
		lowerleft_{0,0},
		width_(2),
		height_(3) 
		{
			// std::cout << "d_ctor class Rectangle\n";
		}

	Rectangle(Point2d const& lowerleft, double const width, double const height, ColorRGB const& color):
		Shape{{color}, "Rectangle", true},
		lowerleft_{lowerleft},
		width_(width),
		height_(height) 
		{
			// std::cout << "ctor class Rectangle\n";
		}

	Rectangle(Point2d const& lowerleft, double const width, double const height, ColorRGB const& color, std::string const& name):
		Shape{{color}, name, true},
		lowerleft_{lowerleft},
		width_(width),
		height_(height) 
		{
			// std::cout << "ctor class Rectangle\n";
		}

	~Rectangle(){/*std::cout <<"dtor class Rectangle\n";*/}

	void lowerleft(Point2d const& lowerleft);
	Point2d lowerleft() const;

	void width(double const width);
	double width() const;

	void height(double const height);
	double height() const;

	void color(ColorRGB const& color);
	ColorRGB color() const;

	double circumference() const;

	void draw(Window const& win) const override;

	void draw(Window const& win, ColorRGB const& clr) const override	;

	bool is_inside(Point2d const& p) const override;

	void translate(double const x, double const y);

	std::ostream& print(std::ostream& os) const override;


private:
	Point2d lowerleft_;
	double width_;
	double height_;
};

#endif