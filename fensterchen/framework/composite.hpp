#ifndef BUW_COMPOSITE
#define BUW_COMPOSITE 

#include "shape.hpp"
#include <vector>

class Composite : public Shape
{
public:
	Composite():
		Shape{{0}, "Composite", false},
		vec_(0) {}

	~Composite(){}

	bool is_inside(Point2d const& p) const override;
	void draw(Window const& win) const override;
	void draw(Window const& win, ColorRGB const& clr) const override;

	void draw_leaf(Window const& win, Point2d const& ptr, ColorRGB const& clr) const override;

	void add(std::shared_ptr<Shape> const& s);
	void remove(std::string const& name);
	std::ostream& print(std::ostream& os) const override;


private:
	std::vector<std::shared_ptr<Shape> > vec_;
};
#endif	