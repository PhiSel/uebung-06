#ifndef BUW_CIRCLE_HPP 
#define BUW_CIRCLE_HPP

#include "shape.hpp"


class Circle : public Shape
{
public:
	Circle():
		Shape{{0},"Circle", true},
		center_{0,0},
		radius_(1.0) 
		{
			// std::cout << "d_ctor class Circle " << name_  << "\n";
		} 

	Circle(Point2d const& center, double radius, ColorRGB const& color):
		Shape{{color}, "Circle", true},
		center_(center),
		radius_{fabs(radius)} 
		{
			// std::cout << "ctor class Circle  " << name_  << "\n";
		}

	Circle(Point2d const& center, double radius, ColorRGB const& color, std::string name):
		Shape{{color}, {name}, true},
		center_(center),
		radius_{fabs(radius)} 
		{
			// std::cout << "ctor class Circle  " << name_  << "\n";
		}

	~Circle(){/*std::cout << "dtor class Circle " << name_  << "\n";*/}

	void center(Point2d const& center); //setter
	Point2d center() const;				//getter		

	void radius(double r);				//setter
	double radius() const;					//getter		

	void color(ColorRGB const& color);	//setter
	ColorRGB color() const;				//getter	

	double circumference() const;	

	void draw(Window const& win) const override;

	void draw(Window const& win, ColorRGB const& clr) const override;

	bool is_inside(Point2d const& p) const override;

	std::ostream& print(std::ostream& os) const override;


private:
	Point2d center_;
	double radius_;
};

#endif