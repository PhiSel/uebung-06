#include "composite.hpp"
#include <vector>

void Composite::add(std::shared_ptr<Shape> const& s)
{
	vec_.push_back(s);
}

void Composite::remove(std::string const& name)
{
	vec_.erase(std::remove_if(vec_.begin(), vec_.end(), 
		[&name](std::shared_ptr<Shape> const& s) 
		{return s -> name() == name;}), vec_.end());
	
}

bool Composite::is_inside(Point2d const& p) const 
{
	for(auto const& item : vec_)
	{
		if(item -> is_inside(p)){return true;}
	}
	return false;
}
	
void Composite::draw(Window const& win) const
{
	for(auto const& item : vec_)
	{
		item -> draw(win);
	}
}

void Composite::draw(Window const& win, ColorRGB const& clr) const
{
	for(auto const& item : vec_)
	{
		item -> draw(win, clr);
	}
}

std::ostream& Composite::print(std::ostream& os) const
{
	for(auto const& item : vec_)
	{
		item -> print(os);
	}
	return os;
}

void Composite::draw_leaf(Window const& win, Point2d const& ptr, ColorRGB const& clr) const
{
	for(auto const& item : vec_)
	{
		if (item -> is_leaf() == false)
		{
			item -> draw_leaf(win, ptr, clr);
		}
		else if (item -> is_inside(ptr))
		{
			item -> draw(win, clr);
		}
		else {item -> draw(win);}
	}
}