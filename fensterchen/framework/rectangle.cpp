#include "rectangle.hpp"
#include "fensterchen.hpp"


void Rectangle::lowerleft(Point2d const& lowerleft)
{
	lowerleft_ = lowerleft;
}

Point2d Rectangle::lowerleft() const 
{
	return lowerleft_;
}

void Rectangle::width(double const width){
	width_ = width;
}

double Rectangle::width() const
{
	return width_;
}

void Rectangle::height(double const height)
{
	height_ = height;
}

double Rectangle::height() const 
{
	return height_;
}

void Rectangle::color(ColorRGB const& color)
{
	color_ = color;
}

ColorRGB Rectangle::color() const 
{
	return color_;
}

double Rectangle::circumference() const
{
	return 2 * width_ + 2 * height_;
}

void Rectangle::draw(Window const& win) const  
{
	win.drawLine(lowerleft_.x(), lowerleft_.y(), 
				 lowerleft_.x() + width_, lowerleft_.y(), color_.r_, color_.g_, color_.b_);
    win.drawLine(lowerleft_.x() + width_, lowerleft_.y(),
    		 	 lowerleft_.x() + width_, lowerleft_.y() + height_, color_.r_, color_.g_, color_.b_);
    win.drawLine(lowerleft_.x() + width_, lowerleft_.y() + height_, 
    			 lowerleft_.x(), lowerleft_.y() + height_, color_.r_, color_.g_, color_.b_);
    win.drawLine(lowerleft_.x(), lowerleft_.y() + height_, 
    			 lowerleft_.x(), lowerleft_.y(), color_.r_, color_.g_, color_.b_);
}

void Rectangle::draw(Window const& win, ColorRGB const& clr) const 
{
	win.drawLine(lowerleft_.x(), lowerleft_.y(), 
				 lowerleft_.x() + width_, lowerleft_.y(), clr.r_, clr.g_, clr.b_);
    win.drawLine(lowerleft_.x() + width_, lowerleft_.y(),
    			 lowerleft_.x() + width_, lowerleft_.y() + height_, clr.r_, clr.g_, clr.b_);
    win.drawLine(lowerleft_.x() + width_, lowerleft_.y() + height_, 
    			 lowerleft_.x(), lowerleft_.y() + height_, clr.r_, clr.g_, clr.b_);
    win.drawLine(lowerleft_.x(), lowerleft_.y() + height_, 
    			 lowerleft_.x(), lowerleft_.y(), clr.r_, clr.g_, clr.b_);
}

bool Rectangle::is_inside(Point2d const& p) const
{
	return(p.x() >= lowerleft_.x() && p.x() <= lowerleft_.x() + width_ &&
	       p.y() >= lowerleft_.y() && p.y() <= lowerleft_.y() + height_);
}

void Rectangle::translate(double const x, double const y)
{
	lowerleft_.translate(x,y);
}

std::ostream& Rectangle::print(std::ostream& os) const
{
	Shape::print(os);
	os	<< ", lowerleft point: (" << lowerleft_.x() << "," << lowerleft_.y() << "), "
		<< "width: " << width_
		<< ", height:  " << height_
		<< std::endl;
	return os;
}