#ifndef BUW_POINT2D_HPP 
#define BUW_POINT2D_HPP

#include <iostream>

class Point2d
{
public:
	Point2d(): 		//Standardkonstruktor
		x_(0),
		y_(0) 
		{
			// std::cout << "d_ctor class Point2d\n";
		}

	Point2d(double const x, double const y):
		x_(x),
		y_(y) 
		{
			// std::cout << "ctor class Point2d\n";
		}

	~Point2d(){/*std::cout << "dtor class Point2d\n";*/}

	double x() const;		//Getter
	double y() const;		//Getter
	void translate(double const x, double const y);		//Setter
	void rotate(double const r);		//Setter
	void rotate(Point2d const& Zentrum, double const r);

private:
	double x_;
	double y_;
};



#endif