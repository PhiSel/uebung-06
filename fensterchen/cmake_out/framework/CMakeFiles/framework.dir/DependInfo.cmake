# The set of languages for which implicit dependencies are needed:
SET(CMAKE_DEPENDS_LANGUAGES
  "C"
  "CXX"
  )
# The set of files for implicit dependencies of each language:
SET(CMAKE_DEPENDS_CHECK_C
  "/Users/philippseltmann/Documents/Schule/Bauhaus-Universitaet-Weimar/SS 14/Programmiersprachen/Uebung 06/fensterchen/framework/glew.c" "/Users/philippseltmann/Documents/Schule/Bauhaus-Universitaet-Weimar/SS 14/Programmiersprachen/Uebung 06/fensterchen/cmake_out/framework/CMakeFiles/framework.dir/glew.c.o"
  )
SET(CMAKE_C_COMPILER_ID "Clang")
SET(CMAKE_DEPENDS_CHECK_CXX
  "/Users/philippseltmann/Documents/Schule/Bauhaus-Universitaet-Weimar/SS 14/Programmiersprachen/Uebung 06/fensterchen/framework/circle.cpp" "/Users/philippseltmann/Documents/Schule/Bauhaus-Universitaet-Weimar/SS 14/Programmiersprachen/Uebung 06/fensterchen/cmake_out/framework/CMakeFiles/framework.dir/circle.cpp.o"
  "/Users/philippseltmann/Documents/Schule/Bauhaus-Universitaet-Weimar/SS 14/Programmiersprachen/Uebung 06/fensterchen/framework/composite.cpp" "/Users/philippseltmann/Documents/Schule/Bauhaus-Universitaet-Weimar/SS 14/Programmiersprachen/Uebung 06/fensterchen/cmake_out/framework/CMakeFiles/framework.dir/composite.cpp.o"
  "/Users/philippseltmann/Documents/Schule/Bauhaus-Universitaet-Weimar/SS 14/Programmiersprachen/Uebung 06/fensterchen/framework/point2d.cpp" "/Users/philippseltmann/Documents/Schule/Bauhaus-Universitaet-Weimar/SS 14/Programmiersprachen/Uebung 06/fensterchen/cmake_out/framework/CMakeFiles/framework.dir/point2d.cpp.o"
  "/Users/philippseltmann/Documents/Schule/Bauhaus-Universitaet-Weimar/SS 14/Programmiersprachen/Uebung 06/fensterchen/framework/rectangle.cpp" "/Users/philippseltmann/Documents/Schule/Bauhaus-Universitaet-Weimar/SS 14/Programmiersprachen/Uebung 06/fensterchen/cmake_out/framework/CMakeFiles/framework.dir/rectangle.cpp.o"
  "/Users/philippseltmann/Documents/Schule/Bauhaus-Universitaet-Weimar/SS 14/Programmiersprachen/Uebung 06/fensterchen/framework/shape.cpp" "/Users/philippseltmann/Documents/Schule/Bauhaus-Universitaet-Weimar/SS 14/Programmiersprachen/Uebung 06/fensterchen/cmake_out/framework/CMakeFiles/framework.dir/shape.cpp.o"
  "/Users/philippseltmann/Documents/Schule/Bauhaus-Universitaet-Weimar/SS 14/Programmiersprachen/Uebung 06/fensterchen/framework/window.cpp" "/Users/philippseltmann/Documents/Schule/Bauhaus-Universitaet-Weimar/SS 14/Programmiersprachen/Uebung 06/fensterchen/cmake_out/framework/CMakeFiles/framework.dir/window.cpp.o"
  )
SET(CMAKE_CXX_COMPILER_ID "Clang")

# Preprocessor definitions for this target.
SET(CMAKE_TARGET_DEFINITIONS
  "FENSTERCHEN_BINARY_DIR=\"/Users/philippseltmann/Documents/Schule/Bauhaus-Universitaet-Weimar/SS 14/Programmiersprachen/Uebung 06/fensterchen/cmake_out\""
  "FENSTERCHEN_SOURCE_DIR=\"/Users/philippseltmann/Documents/Schule/Bauhaus-Universitaet-Weimar/SS 14/Programmiersprachen/Uebung 06/fensterchen\""
  "GLEW_STATIC"
  )

# Targets to which this target links.
SET(CMAKE_TARGET_LINKED_INFO_FILES
  )

# The include file search paths:
SET(CMAKE_C_TARGET_INCLUDE_PATH
  "../external"
  "../external/glfw-3.0.3/include"
  "../external/glm-0.9.5.3"
  "../framework"
  "../framework/.."
  )
SET(CMAKE_CXX_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
SET(CMAKE_Fortran_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
SET(CMAKE_ASM_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
