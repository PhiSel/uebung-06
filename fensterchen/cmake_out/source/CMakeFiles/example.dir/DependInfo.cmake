# The set of languages for which implicit dependencies are needed:
SET(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
SET(CMAKE_DEPENDS_CHECK_CXX
  "/Users/philippseltmann/Documents/Schule/Bauhaus-Universitaet-Weimar/SS 14/Programmiersprachen/Uebung 06/fensterchen/source/example.cpp" "/Users/philippseltmann/Documents/Schule/Bauhaus-Universitaet-Weimar/SS 14/Programmiersprachen/Uebung 06/fensterchen/cmake_out/source/CMakeFiles/example.dir/example.cpp.o"
  )
SET(CMAKE_CXX_COMPILER_ID "Clang")

# Preprocessor definitions for this target.
SET(CMAKE_TARGET_DEFINITIONS
  "FENSTERCHEN_BINARY_DIR=\"/Users/philippseltmann/Documents/Schule/Bauhaus-Universitaet-Weimar/SS 14/Programmiersprachen/Uebung 06/fensterchen/cmake_out\""
  "FENSTERCHEN_SOURCE_DIR=\"/Users/philippseltmann/Documents/Schule/Bauhaus-Universitaet-Weimar/SS 14/Programmiersprachen/Uebung 06/fensterchen\""
  "GLEW_STATIC"
  )

# Targets to which this target links.
SET(CMAKE_TARGET_LINKED_INFO_FILES
  "/Users/philippseltmann/Documents/Schule/Bauhaus-Universitaet-Weimar/SS 14/Programmiersprachen/Uebung 06/fensterchen/cmake_out/framework/CMakeFiles/framework.dir/DependInfo.cmake"
  "/Users/philippseltmann/Documents/Schule/Bauhaus-Universitaet-Weimar/SS 14/Programmiersprachen/Uebung 06/fensterchen/cmake_out/external/glfw-3.0.3/src/CMakeFiles/glfw.dir/DependInfo.cmake"
  )

# The include file search paths:
SET(CMAKE_C_TARGET_INCLUDE_PATH
  "../external"
  "../external/glfw-3.0.3/include"
  "../external/glm-0.9.5.3"
  "../framework"
  )
SET(CMAKE_CXX_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
SET(CMAKE_Fortran_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
SET(CMAKE_ASM_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
