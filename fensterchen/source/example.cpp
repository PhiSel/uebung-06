#include "fensterchen.hpp"
#include "shape.hpp"
#include "circle.hpp"
#include "rectangle.hpp"
#include "composite.hpp"
#include <iostream>


int main(int argc, char* argv[])
{
  ColorRGB red(255, 0, 0); 
  ColorRGB blue(0,0,255);
  Point2d p(0,0);

  /*************
  ** Statisch Klasse:
  ** Die Klasse, die bei der Deklaration einer Variablen verwendet wird
  **
  ** Dynamische KLasse:
  ** Die tatsaechliche Klasse des Wertes, der durch die Variable referenziert wird
  ** 
  ** Erlaubt statisch typisierte Sprachen wie C++ (in der Vererbung), 
  ** dass die dynamische Klasse einer Variablen einer von der 
  ** statischen Klasse abgeleitete Klasse sein kann
  **
  ** Daraus folgt, dass eine Variable der Basisklasse Objekte
  ** einer abgeleiteten Klasse referenzieren kann
  **************/

#if 1
  // statische Klasse von s1 ist Circle, dynamische KLasse ist Circle
  std::shared_ptr<Circle> s1 = std::make_shared<Circle>(p, 1.2, red, "sphere0");
  // statische KLasse von s2 ist Shape, dynamische Klasse ist Circle
  std::shared_ptr<Shape> s2 = std::make_shared<Circle>(p, 1.2, red, "sphere1");
  s1->print(std::cout); 
  s2->print(std::cout);

#else
  Shape* s1 = new Circle(p, 1.2, red, "sphere0");
  Shape* s2 = new Circle(p, 1.2, red, "sphere1");
  s1 -> print(std::cout);
  s2 -> print(std::cout);

  delete s1;
  delete s2;
#endif

  std::cout << "\n";

  Point2d p2(0.5,0.5);

  std::shared_ptr<Shape> c1 = std::make_shared<Circle>(Point2d(0.2,0.2), 0.1, red, "c1");

  std::shared_ptr<Shape> c2 = std::make_shared<Circle>(Point2d(0.8,0.2), 0.1, red, "c2");
  std::shared_ptr<Shape> r1 = std::make_shared<Rectangle>(Point2d(0.2,0.2), 0.6, 0.2, red, "r1");
  std::shared_ptr<Shape> r2 = std::make_shared<Rectangle>(Point2d(0.3,0.4), 0.35, 0.1, red, "r2");
  std::shared_ptr<Shape> r3 = std::make_shared<Rectangle>(Point2d(0.32,0.43), 0.05, 0.05, red, "r3");
  std::shared_ptr<Shape> r4 = std::make_shared<Rectangle>(Point2d(0.5,0.43), 0.05, 0.05, red, "r4");

  std::shared_ptr<Shape> r5 = std::make_shared<Rectangle>(p, 0.2, 0.9, red, "r5");
  std::shared_ptr<Shape> r6 = std::make_shared<Rectangle>(p, 0.2, 0.9, red, "r5");

  Composite Reifen{};
  Reifen.add(c1);
  Reifen.add(c2);

  Composite Rechtecke{};
  Rechtecke.add(r1);
  Rechtecke.add(r2);

  Composite Fenster{};
  Fenster.add(r3);
  Fenster.add(r4);


  std::shared_ptr<Shape> Reifen_ptr = std::make_shared<Composite>(Reifen);
  std::shared_ptr<Shape> Rechtecke_ptr = std::make_shared<Composite>(Rechtecke);
  std::shared_ptr<Shape> Fenster_ptr = std::make_shared<Composite>(Fenster);
  
  Composite Automobil{};
  Automobil.add(r5);
  Automobil.add(r6);
  Automobil.add(Reifen_ptr);
  Automobil.add(Rechtecke_ptr);
  Automobil.add(Fenster_ptr);

  std::cout << Automobil << "\n";

  Automobil.remove("r5");
  std::cout << Automobil << "\n";



  Window win(glm::ivec2(600,600));

  while (!win.shouldClose()) {
    if (win.isKeyPressed(GLFW_KEY_ESCAPE)) {
      win.stop();
    }

    auto t = win.getTime();
    float x1(0.5 + 0.5 * std::sin(t)); float y1(0.5 + 0.5 * std::cos(t));
    float x2(0.5 + 0.5 * std::sin(2.0*t)); float y2(0.5 + 0.5 * std::cos(2.0*t));
    float x3(0.5 + 0.5 * std::sin(t-10.f)); float y3(0.5 + 0.5 * std::cos(t-10.f));

    win.drawPoint(x1, y1, 255, 0, 0);
    win.drawPoint(x2, y2, 0, 255, 0);
    win.drawPoint(x3, y3, 0, 0, 255);

    auto m = win.mousePosition();
    // win.drawLine(0.1f,0.1f, 0.8f,0.1f, 255,0,0);

    // win.drawLine(0.0f, m.y, 0.01f, m.y, 0,0,0);
    // win.drawLine(0.99f, m.y,1.0f, m.y, 0,0,0);

    // win.drawLine(m.x, 0.0f, m.x, 0.01f, 0,0,0);
    // win.drawLine(m.x, 0.99f, m.x, 1.0f, 0,0,0);

    //Begrenzungsrahmen (beim Mac unteres linkes Viertel)
    win.drawLine(1.0f, 0.0f, 1.0f, 1.0f, 0, 0, 0);
    win.drawLine(0.0, 1.0f, 1.0f, 1.0f, 0, 0, 0);


    // if(Automobil.is_inside({m.x, m.y})) { Automobil.draw(win,blue); }
    // else {Automobil.draw(win);}

    Automobil.draw_leaf(win,Point2d(m.x,m.y), blue);
    // Automobil.draw(win);

    win.update();
  }

  return 0;
}
